const std = @import("std");
const FileSource = std.Build.FileSource;

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardOptimizeOption(.{});

    //const lib = b.addStaticLibrary("zig_compact", "src/main.zig");
    //lib.setBuildMode(mode);
    //lib.install();

    const main_tests = b.addTest(.{
        .name = "main",
        .root_source_file = FileSource.relative("src/main.zig"),
        .target = target,
        .optimize = mode,
    });

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);
}
